import tensorflow.keras as tf


def structure_data():
    pass


def create_model(inputs, outputs):
    return tf.Model(inputs=inputs, outputs=outputs)


def generate_new_prediction(model):
    yield model.predict()


def main():


if __name__ == '__main__':
    main()
